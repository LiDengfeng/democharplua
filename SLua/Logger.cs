

using System;

namespace SLua
{

	public enum SLuaLogType
	{
		Error,
		Assert,
		Warning,
		Log,
		Exception
	}

    /// <summary>
    /// A bridge between UnityEngine.Debug.LogXXX and standalone.LogXXX
    /// </summary>
    public class Logger
    {
		public delegate void LogCallback (string condition, string stackTrace, SLuaLogType type);
		public static event LogCallback logMessageReceived;

        public static void Log(string msg)
        {
            Console.WriteLine(msg);
			if(logMessageReceived != null)
				logMessageReceived(msg, "", SLuaLogType.Log);
        }
        public static void LogError(string msg)
        {
            Console.WriteLine(msg);
			if(logMessageReceived != null)
				logMessageReceived(msg, "", SLuaLogType.Error);
        }

		public static void LogWarning(string msg)
		{
            Console.WriteLine(msg);
			if(logMessageReceived != null)
				logMessageReceived(msg, "", SLuaLogType.Warning);
		}

		public static void LogException(System.Exception ex, System.Object context = null)
		{
			if(null == context)
				Console.WriteLine("Exception:" + ex.Message);
			else
				Console.WriteLine("Exception:" + ex.Message + ", " + context.ToString());

			if(logMessageReceived != null)
				logMessageReceived(ex.Message, "", SLuaLogType.Exception);
		}
    }
}