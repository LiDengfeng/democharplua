﻿namespace LuaInterface
{
    using System;
    using System.Runtime.InteropServices;
    using System.Reflection;
    using System.Collections;
    using System.Text;
    using System.Security;
    using SLua;

    public class LuaDLLNativeRuntime
    {
        const string LUADLL = LuaDLL.LUADLL;

        public static void Establish(IntPtr L)
        {
            L_EstablishAnyLog(On_SLua_AnyLog);
            L_SetupLuaState(L);
        }

        public static void UnEstablish()
        {
            L_CleanupLuaState();
            L_UnEstablishAnyLog();
        }
			
        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void L_CleanupLuaState();

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public static extern void L_SetupLuaState(IntPtr luaState);

        public delegate void SLua_AnyLog_Delegate(SLuaLogType logType, string message);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public extern static void L_EstablishAnyLog(SLua_AnyLog_Delegate func);

        [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
        public extern static void L_UnEstablishAnyLog();

        [MonoPInvokeCallback(typeof(SLua_AnyLog_Delegate))]
        private static void On_SLua_AnyLog(SLuaLogType logType, string message)
        {
            switch(logType)
            {
                case SLuaLogType.Log:
					Logger.Log("["+LUADLL+"]"+message);
                    break;
                case SLuaLogType.Warning:
					Logger.LogWarning("["+LUADLL+"]"+message);
                    break;
                case SLuaLogType.Error:
                    Logger.LogError("["+LUADLL+"]"+message);
                    break;
                case SLuaLogType.Exception:
					Logger.LogException(new Exception("["+LUADLL+"]"+message));
                    break;
                case SLuaLogType.Assert:
					Logger.LogError("["+LUADLL+"]"+message);
                    break;
            }
        }
    }
}
