﻿// The MIT License (MIT)

// Copyright 2015 Siney/Pangweiwei siney@yeah.net
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// uncomment this will use static binder(class BindCustom/BindUnity), 
// init will not use reflection to speed up the speed
//#define USE_STATIC_BINDER  

namespace SLua
{
	using System;
	using System.IO;
	using System.Threading;
	using System.Collections;
	using System.Collections.Generic;
	using LuaInterface;
	using System.Reflection;

	public class LuaSvr 
	{
		public LuaState luaState;

		int errorReported = 0;
		public bool inited = false;

        public static LuaSvr main = null;

		public string workspace = System.IO.Directory.GetCurrentDirectory();

        public LuaSvr(string workspace_ = null)
		{
			LuaState luaState = new LuaState();
            this.luaState = luaState;
			main = this;

			if(!string.IsNullOrEmpty(workspace_))
				workspace = workspace_;

			LuaState.loaderDelegate += LoadAssets;
		}

        public void Close()
        {
            if (!inited)
                return;
            inited = false;
            main = null;
            luaState.Close();
            luaState = null;
        }

		private void doBind(object state)
		{
			/*IntPtr L = (IntPtr)state;

            List<Action<IntPtr>> list = new List<Action<IntPtr>>();
            
		    var assemblyName = "Assembly-CSharp";
            Assembly assembly = Assembly.Load(assemblyName);
			list.AddRange(getBindList(assembly,"SLua.BindUnity"));
			list.AddRange(getBindList(assembly,"SLua.BindUnityUI"));
			list.AddRange(getBindList(assembly,"SLua.BindDll"));
			list.AddRange(getBindList(assembly,"SLua.BindCustom"));
	
			int count = list.Count;
			for (int n = 0; n < count; n++)
			{
				Action<IntPtr> action = list[n];
				action(L);
			}*/
		}

		Action<IntPtr>[] getBindList(Assembly assembly,string ns) {
			Type t=assembly.GetType(ns);
			if(t!=null)
				return (Action<IntPtr>[]) t.GetMethod("GetBindList").Invoke(null, null);
			return new Action<IntPtr>[0];
		}

        void doinit(IntPtr L)
		{
			LuaHelper.reg(L);
			//LuaValueType.reg(L);
            LuaRegister.reg(L);

			LuaDLL.luaS_openextlibs(L);
			Lua3rdDLL.open(L);

            inited = true;
		}

		void checkTop(IntPtr L)
		{
			if (LuaDLL.lua_gettop(luaState.L) != errorReported)
			{
				Logger.LogError("Some function not remove temp value from lua stack. You should fix it.");
				errorReported = LuaDLL.lua_gettop(luaState.L);
			}
		}

		public void init()
        {
            IntPtr L = luaState.L;
			LuaObject.init(L);

            doBind(L);
            doinit(L);
            checkTop(L);
        }

		public object start(string main, params object[] args)
		{
			if (main != null)
			{
				luaState.doFile(main);
				LuaFunction func = (LuaFunction)luaState["main"];
				if(func!=null)
					return func.call(args);
			}
			return null;
		}

		byte[] LoadAssets(string fn)
		{
			string fullname = fn;
			if(!string.IsNullOrEmpty(workspace))
				fullname = Path.Combine(workspace, fn);

			if(File.Exists(fullname))
			{
				return File.ReadAllBytes(fullname);
			}
			return null;
		}
	}
}
