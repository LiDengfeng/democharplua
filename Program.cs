﻿using System;
using System.Runtime.InteropServices;
using LuaInterface;
using SLua;

namespace demo
{
    class Program
    {

        const string LUADLL = LuaDLL.LUADLL;

        [DllImport("lua_wrapper", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr luaL_newstate();

        [DllImport("lua_wrapper", CallingConvention = CallingConvention.Cdecl)]
        public static extern int luaL_loadfile(IntPtr luaState, string filename);


        public delegate void SLua_AnyLog_Delegate(int logType, string message);

        [DllImport("lua_wrapper", CallingConvention = CallingConvention.Cdecl)]
        public extern static void exp_init_UnityLog(SLua_AnyLog_Delegate func);

        [DllImport("lua_wrapper")]
        public static extern void exp_testLog(int a);


        [MonoPInvokeCallback(typeof(SLua_AnyLog_Delegate))]
        private static void On_SLua_AnyLog(int logType, string message)  
        {
            Console.WriteLine("On_SLua_AnyLog = " + message);
        }      
        static void Main(string[] args)
        {
            // exp_init_UnityLog(On_SLua_AnyLog);
            // Console.ForegroundColor = ConsoleColor.DarkGreen;
            // Console.WriteLine("Hello World!");
            // IntPtr lvm = luaL_newstate();
            // Console.WriteLine("luaVM = " + lvm);

            // Console.WriteLine(luaL_loadfile(lvm, "/Volumes/SHARED/WorkSpace/Lua/lua_wrapper/macosx/test.lua"));

            // exp_testLog(0);

            LuaSvr lua = new LuaSvr();
            lua.init();
            lua.start("entry.lua",args);

            lua.Close();
        }
    }
}
